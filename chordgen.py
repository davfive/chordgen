###########################################################################################
# Here is the file with the known guitar chords
#   https://github.com/Tembrel/lilypond/blob/master/ly/predefined-guitar-fretboards.ly
#
# What chords are supports?
#   This about which keys on the piano have black keys surrounding them
#     black to the left gets *es, black to the right gets *is
#
#   c cis, des d dis, ees e, f fis, ges g gis, aes a ais, bes b 
#     :m,aug,dim,dim7,7,maj7,m7
#
#-----------------------------------------------------------------------------------------
# To install this, you need to install Lilypond first. LILYPOND_BIN is setup for use 
# on a Mac, if you are running Windows or Linux, then change it to your lilypond
# commandline path
#
# Note that this script has only been tested on a Mac.
###########################################################################################

from __future__ import print_function
import argparse
import os
import glob
import pathlib
import tempfile
import shutil

LILYPOND_BIN = '/Applications/LilyPond.app/Contents/Resources/bin/lilypond'
LILYPOND_PARAMS = '--silent --png -dbackend=eps -dresolution=600' # -dresolution makes the png big enough to be usable
LILYPOND_GARBAGE = ['-systems.count', '-1.eps', '-systems.tex', '-systems.texi', '.eps']

def get_basename(chords):
	basename = chords.replace('#', 's').replace('/', '_').replace(' ','_')
	return basename

def generate_lilyfile(lilyfile_path, chords):
	with open(lilyfile_path, 'w') as lyout:
		lyout.writelines("""
\\include "lilypond-book-preamble.ly"
\\include "predefined-guitar-fretboards.ly"
mychords = \\chordmode {{
  {}
}}

mychordlist = {{
  \\mychords
}}
<<
  \\new ChordNames {{
	\\mychordlist
  }}
  \\new FretBoards {{
	\\mychordlist
  }}
>>
""".format(chords))
	
def generate_pngfile(lilyfile_path, pngpath_basename):
	# call the png creator on the lilypond path (devnull it to kill the annoying warning)
	os.system('%(lilypond)s %(params)s -o %(output)s %(input)s > /dev/null 2>&1' % {
		'lilypond': LILYPOND_BIN,
		'params': LILYPOND_PARAMS,
		'output': pngpath_basename,
		'input': lilyfile_path,
	})

def generate(outdir, chords):
	# Setup all of the files
	tmpdir = tempfile.mkdtemp(prefix='tmp-lily')

	basename = get_basename(chords)
	tmp_basename = "{}/{}".format(tmpdir, basename)
	out_basename = "{}/{}".format(outdir, basename)

	lilyfile_path = "{}.ly".format(tmp_basename)

	pngtmp_basename = tmp_basename
	pngtmp_path = "{}.png".format(tmp_basename) 
	pngout_path = "{}.png".format(out_basename) 

	# Create Lily Markup File
	generate_lilyfile(lilyfile_path, chords)

	# Generate PNG Image File
	generate_pngfile(lilyfile_path, pngtmp_basename)

	# Move png from tmp to output directory
	shutil.move(pngtmp_path, pngout_path)

	# Cleanup temp directory (safe way, always worried about shutil.rmtree doing "*" or something)
	for tmpfile in glob.glob("{}*".format(tmp_basename)):
		os.remove(tmpfile)
	os.rmdir(tmpdir)

	return pngout_path

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description="""
Create guitar chord chart png files.
What chords are supports?
  This about which keys on the piano have black keys surrounding them:
    = black to the left gets *es, black to the right gets *is

   c cis des d dis ees e f fis ges g gis aes a ais bes b 
    +  :m,aug,dim,dim7,7,maj7,m7
   e.g., c:m or 'c d g g:7'
""")
	parser.add_argument('--outdir', default=os.path.expanduser('~/Downloads'))
	parser.add_argument('chords', help="String naming chords.")
	args = parser.parse_args()

	pngout_path = generate(args.outdir, args.chords)
	print("PNG at {}".format(pngout_path))